<?php

require_once dirname(__FILE__) . '/../BankAccount.php';

class BankAccountTest  extends PHPUnit_Framework_TestCase
{
	public function testDeposit()
	{
		$acc = new BankAccount();
		$acc->deposit(50);
		$this->assertEquals($acc->getBalance(), 50);
	}
	
	public function testWithdraw()
	{
		$acc = new BankAccount(75);

		$acc->withdraw(50);
		$this->assertEquals($acc->getBalance(), 25);
	}

	public function testWithdrawWithPenalty()
	{
		$acc = new BankAccount(10);

		$acc->withdraw(20);
		$this->assertEquals($acc->getBalance(), -15);
	}
}