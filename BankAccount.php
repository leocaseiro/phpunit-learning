<?php

class BankAccount
{
	private $balance = 0;

	function __construct($money = 0)
	{
		$this->balance = $money;
	}

	public function getBalance()
	{
		return $this->balance;
	}

	public function deposit($money)
	{
		$this->balance += $money;
	}

	public function withdraw($money)
	{
		$this->balance -= $money;

		if ($this->balance < 0)
		{
			$this->balance -= 5;
		}
	}
}