<?php
$path = '/Applications/MAMP/htdocs/wordpress-develop/tests/phpunit/includes/bootstrap.php';

if( file_exists( $path ) ) {
	$GLOBALS['wp_tests_options'] = array(
		'active_plugins' => array('bank-account/bank-account.php')
	);
    require_once $path;
} else {
    exit( "Couldn't find path to wordpress-develop/bootstrap.php\n" );
}