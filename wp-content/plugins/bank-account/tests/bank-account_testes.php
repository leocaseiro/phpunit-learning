<?php

require_once dirname(__FILE__) . '/../bank-account.php';

class BankAccount_Tests extends WP_UnitTestCase {

	public function setUp() {
		parent::setUp();

		$acc = new BankAccount();
		return $acc;
	}

	public function testDeposit()
	{
		$acc = self::setUp();
		$acc->deposit(50);
		$this->assertEquals($acc->getBalance(), 50);
	}
	
	public function testWithdraw()
	{
		$acc = self::setUp();
		$acc->deposit(75);
		
		$acc->withdraw(50);
		$this->assertEquals($acc->getBalance(), 25);
	}

	public function testWithdrawWithPenalty()
	{
		$acc = self::setUp();
		$acc->deposit(10);
		
		$acc->withdraw(20);
		$this->assertEquals($acc->getBalance(), -15);
	}

}
