<?php
/*
Plugin Name: Bank Account
Plugin URI: #
Description: A simple plugin used to help demonstrate unit testing in the context of WordPress.
Version: 1.0
Author: Leo Caseiro
Author URI: http://leocaseiro.com.br
Author Email: leobok@gmail.com
*/

class BankAccount {
	
	private $balance = 0;

	function __construct($money = 0)
	{
		$this->balance = $money;
	}

	public function getBalance()
	{
		return $this->balance;
	}

	public function deposit($money)
	{
		$this->balance += $money;
	}

	public function withdraw($money)
	{
		$this->balance -= $money;

		if ($this->balance < 0)
		{
			$this->balance -= 5;
		}
	}
}